// imports dependencies
import { Router } from "express"


import { connectDbAndRunQueries } from './dbOps.js'
const router = Router()



router.post('/getrestaurants/:item?', (req, res) => {
	connectDbAndRunQueries('getRestaurants', req, res)

})

export default router
