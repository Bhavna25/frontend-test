## Installation

Clone the repository: 
```
git clone https://gitlab.com/Bhavna25/frontend-test.git
``` 
Navigate inside the directory:
```
cd react-app-restaurant-app
```
Install all the necessary dependecies:
```
npm install
``` 
Navigate to the directory:
```
cd server
```
Run the server
```
npm run server
```
Open the web browser and type`http://localhost:3000` in the address bar to load the application 


